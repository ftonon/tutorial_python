# coding: utf-8
import os
import json
from django.core.management.base import BaseCommand, CommandError

from materiais.models import GrupoDeMaterial

class Command(BaseCommand):
    args = 'caminho'
help = 'Importa grupos em um json no banco de dados'

    def handle(self, *args, **options):
        if not args:
            raise CommandError('Nenhum arquivo informado.')
        caminho = args[0]

        if not os.path.exists(caminho):
            msg = 'Arquivo informado ({}) não existe.'.format(caminho)
            raise CommandError(msg)

        with open(caminho) as f:
            dados = json.loads(f.read())

        for item in dados:
            pass