
Instalação
==========

Utilizaremos diversas bibliotecas de terceiros para as tarefas deste projeto.


Instalando o Sphinx
-------------------

Ferramenta que torna fácil a documentação de projetos Python, é amplamente
utilizada pela comunidade. Há um serviço gratuíto para hospedagem de
documentação neste formato para projetos open source, o http://readthedocs.org

O Sphinx é uma ferramenta multiprojeto, então você pode instalar em seu
ambiente Python sem preocupação com dependências ou versão específica.

Utilize o pip::

    $ pip install Sphinx


Instalando o rst2pdf
--------------------

Ferramenta cross-platform para gerar PDFs a partir de arquivos RST.

Utilize o easy_install::

    $ easy_install rst2pdf


Instalando o Virtualenv
-----------------------

Ferramenta para gerenciamento de multiplos ambientes Python em uma mesma
máquina.

Utilize o pip::

    $ pip install virtualenv
