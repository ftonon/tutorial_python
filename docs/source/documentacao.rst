
Quickstart da documentação
==========================

A partir da raiz de seu projeto, crie um subdiretório **docs**, e execute o
utilitário de setup rápido para documentação de projetos do Sphinx, o
**sphinx-quickstart**::

    $ mkdir docs
    $ cd docs
    $ sphinx-quickstart

Será apresentado um pequeno questionário, seguiremos quase que completamente as opções padrão::

    Welcome to the Sphinx 1.1.3 quickstart utility.
    [...]
    > Root path for the documentation [.]:
    > Separate source and build directories (y/N) [n]: y
    > Name prefix for templates and static dir [_]:
    > Project name: Coolcad
    > Author name(s): <Seu nome>
    > Project version: 0.1
    > Project release [0.1]:
    > Source file suffix [.rst]:
    > Name of your master document (without suffix) [index]:
    > Do you want to use the epub builder (y/N) [n]: y
    > autodoc: automatically insert docstrings from modules (y/N) [n]: y
    > doctest: automatically test code snippets in doctest blocks (y/N) [n]: y
    > intersphinx: link between Sphinx documentation of different projects (y/N) [n]:
    > todo: write "todo" entries that can be shown or hidden on build (y/N) [n]: y
    > coverage: checks for documentation coverage (y/N) [n]: y
    > pngmath: include math, rendered as PNG images (y/N) [n]:
    > mathjax: include math, rendered in the browser by MathJax (y/N) [n]:
    > ifconfig: conditional inclusion of content based on config values (y/N) [n]:
    > viewcode: include links to the source code of documented Python objects (y/N) [n]:
    > Create Makefile? (Y/n) [y]: y
    > Create Windows command file? (Y/n) [y]: y

O Sphinx criou alguns arquivos para nós:

    :Makefile: Arquivo de build estilo Make.
    :make.bat: Arquivo de build adaptado para windows.
    :source/conf.py:  Arquivo de configuração do Sphinx.
    :source/index.rst: Raíz da documentação, com o doctree onde adicionaremos referências aos nossos arquivos.


Edite o arquivo :file:`source/conf.py` para ajustar a linguagem, os templates padrão suportam `várias linguagens`_, entre elas, Português Brasileiro.

Procure a linha :code:`language = None`, e edite como segue::


    language = 'pt_BR'


.. _`várias linguagens`: http://sphinx-doc.org/config.html#confval-language


Para adicionar referências aos arquivos do projeto, bem como gerar
documentação automática dos módulos, precisamos adicionar o diretório dos
fontes :term:`project_root` ao ``sys.path``. Procure a linha
:code:`#sys.path.insert(0, os.path.abspath('.'))` e edite como segue::

    sys.path.insert(0, os.path.abspath('../../project'))



Builder PDF
-----------

Para conseguirmos gerar saída PDF no Windows sem utilizar o LaTex, vamos adicionar um
bloco pdf no arquivo :file:`make.bat`.


.. code-block:: bash


    if "%1" == "pdf" (
        %SPHINXBUILD% -b pdf %ALLSPHINXOPTS% %BUILDDIR%/pdf
        if errorlevel 1 exit /b 1
        echo.
        echo.Build finished. The PDF is in %BUILDDIR%/pdf.
        goto end
    )


Edite o arquivo :file:`source/conf.py` para incluir o builder pdf.

Procure a linha :code:`extensions = [...`, e adicione o builder ``rst2pdf.pdfbuilder``.::

    extensions = [..., 'rst2pdf.pdfbuilder']


E adicione estas chaves ao final do arquivo :file:`source/conf.py` ::

    pdf_documents = [
        ('index', u'Coolcad', u'Coolcad', u'<Seu nome>'),
    ]

    pdf_stylesheets = ['sphinx','kerning','a4']

    pdf_language = "pt_BR"


Para mais detalhes sobre as configurações disponíveis, consulte o `manual <http://rst2pdf.googlecode.com/svn/trunk/doc/manual.txt>`_.


Gerando a documentação
----------------------

Execute o arquivo :file:`make.bat` com nome do builder como primeiro argumento:

.. code-block:: bash

    $ make html

Ou:

.. code-block:: bash

    $ make pdf


Referências
-----------

Você pode encontrar diversos tutoriais sobre reStructuredText na web.

#. `rst_syntax <http://openalea.gforge.inria.fr/doc/openalea/doc/_build/html/source/sphinx/rest_syntax.html>`_
#. `Manual <http://rst2pdf.googlecode.com/svn/trunk/doc/manual.txt>`_
#. Code highlight, `linguagens suportadas <http://pygments.org/languages/>`_