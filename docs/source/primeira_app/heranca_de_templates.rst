
Herança de templates
====================

Vamos estruturar nossa app de materiais, para a partir da listagem, possamos
acessar os detalhes de cada material. Para isso, criaremos um layout base, e uma nova página de detalhes.


Localize o diretório ``templates`` no :term:`project_root`. Nele, crie um
arquivo ``base.html``.

Edite o arquivo ``base.html``, conforme segue:

.. code-block:: django

    <html>
    <head>
        <title>Coolcad</title>
    </head>
    <body>
    <h1>Coolcad</h1>
    <hr/>
    <div id="conteudo">
    {% block conteudo %}
    Conteúdo base
    {% endblock %}
    </div>

    <div>
    {% block rodape %}
    NCR Colibri Solutions
    {% endblock %}

    {%block "js"%}{%endblock%}
    </div>
    </body>
    </html>


Agora vamos refatorar o arquivo :file:`templates/materiais/index.html` para utilizar nosso template base:

.. code-block:: django
    :linenos:

    {% extends "base.html" %}

    {%block conteudo %}
        <h2>Lista de materiais</h2>
        {% if not materiais %}
            <p>Sem materiais para exibir.</p>
        {%else%}
            <ul>
            {% for m in materiais %}
                <li><a href="{% url "detalhe" m.id %}">{{m.descricao}}</a></li>
            {%endfor%}
            </ul>
        {%endif%}
    {%endblock%}


Aponte o browser para ``localhost:8000/materiais``. Você deve ver uma mensagem de erro::

    NoReverseMatch:  ...

O Django está nos dizendo que não conhece uma rota para ``detalhe``, solicitada através da templatetag ``url`` (linha 10), que ainda não criamos.

Nossa configuração de rotas em :file:`coolcad/urls.py` tem uma rota que aponta para a view index, na app materiais::

        url(r'^materiais/', 'materiais.views.index'),

Inclua a rota para uma view ``detalhe``, as rotas para o app materiais devem estar assim:

.. code-block:: python
    :emphasize-lines: 1

    url(r'^materiais/(\d+)', 'materiais.views.detalhe', name="detalhe"),
    url(r'^materiais/$', 'materiais.views.index'),


Vamos adicionar a view ``detalhe``. Edite o arquivo :file:`materiais/views.py`, conforme segue:

.. code-block:: python
    :emphasize-lines: 16-21

    #coding: utf-8

    from django.shortcuts import render
    from .models import Material


    def index(request):
        materiais = Material.objects.all()
        context = dict(
            nome="Colibri",
            materiais=materiais
        )
        return render(request, 'materiais/index.html', context)


    def detalhe(request, material_id):
        material = Material.objects.get(pk=int(material_id))
        context = dict(
            material=material
        )
        return render(request, 'materiais/detalhe.html', context)


E por fim, crie um arquivo ``detalhe.html`` em
``project/templates/materiais``. Edite este arquivo, conforme segue:

.. code-block:: django

    {% extends "base.html" %}

    {%block conteudo %}
        <h2>{{material}}</h2>
        <span>Codigo: {{material.codigo}}</span>
        <br/>
        <span>Descricao: {{material.descricao}}</span>
        <br/>
        <span>Grupo: {{material.grupo}}</span>
    {% endblock %}

Aponte o browser para ``localhost:8000/materiais``. Você deve ver a listagem dos materiais cadastrados no banco, com um link para a página de detalhe do material.
