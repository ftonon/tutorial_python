.. _glossary:

Glossário
=========

.. glossary::


    project_root
        Diretório raiz da aplicação Django. É um subdiretório do :term:`repository_root`.


    repository_root
        Diretório raiz do projeto, normalmente onde se faz o controle de
        versionamento. Nele estão contidos os diretórios de documentação, fontes e
        dependências.
