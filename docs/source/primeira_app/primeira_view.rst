Primeira view
=============

Agora vamos iniciar o front-end. Faremos nossa primeira view.

O Django possui dois sistemas de views, as Function Based Views (FBV) e as Class Based Views (CBV). As CBVs estão disponíveis a partir da versão 1.3 do Django, e não substituem as FBV, apenas apresentam uma interface OO para as views, a escolha fica por conta do desenvolvedor.

A responsabilidade básica de toda view é simples: ela recebe um objeto ``HttpRequest``, e deve retornar um ``HttpResponse``. Vamos fazer uma view básica.

Edite o arquivo :file:`materiais/views.py` como segue::

    #coding: utf-8
    from django.http import HttpResponse

    def index(request):
        return HttpResponse('Primeira view')


Esta view ainda não está disponível ao usuário final, precisamos adicionar uma rota para ela. Abra o arquivo :file:`coolcad/urls.py`, ele deve estar assim ::

    from django.conf.urls import patterns, include, url

    from django.contrib import admin
    admin.autodiscover()

    urlpatterns = patterns('',
        # Examples:
        # url(r'^$', 'coolcad.views.home', name='home'),
        # url(r'^coolcad/', include('coolcad.foo.urls')),

        url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
        url(r'^admin/', include(admin.site.urls)),
    )

Edite para que esteja assim:

.. code-block:: python

    from django.conf.urls import patterns, include, url

    from django.contrib import admin
    admin.autodiscover()

    urlpatterns = patterns('',
        # Examples:
        # url(r'^$', 'coolcad.views.home', name='home'),
        # url(r'^coolcad/', include('coolcad.foo.urls')),
        url(r'^materiais/', 'materiais.views.index'),

        url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
        url(r'^admin/', include(admin.site.urls)),
    )

Execute o servidor de desenvolvimento (se não estiver em execução)::

    (env)$ python manage.py runserver


Aponte o browser para ``localhost:8000/materiais``. Você deve ver a mensagem::

    Primeira view

