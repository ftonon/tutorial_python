
Organizando URLConfs
====================

Em nossa APP de materiais temos duas views, a ``index`` e a ``detalhe``. E para
cada uma definimos uma rota de acesso no URLConf principal do projeto, no
arquivo :file:`coolcad/urls.py`.

No contexto de uma única app, ter uma rota nomeada como ``detalhe`` pode fazer
sentido,  mas para um projeto composto de várias apps, este nome pode
rapidamente se tornar ambíguo.

Outro problema de nossas rotas é que o projeto conhece muito sobre como a
app funciona. Melhor seria ter as rotas da app auto-contidas.

Vamos resolver estas duas questões.


URLConfs da app materiais
-------------------------

Edite as rotas do projeto, seu arquivo :file:`coolcad/urls.py` está assim::

    from django.conf.urls import patterns, include, url

    from django.contrib import admin
    admin.autodiscover()

    urlpatterns = patterns('',
        # Examples:
        # url(r'^$', 'coolcad.views.home', name='home'),
        # url(r'^coolcad/', include('coolcad.foo.urls')),
        url(r'^materiais/(\d+)', 'materiais.views.detalhe', name="detalhe"),
        url(r'^materiais/$', 'materiais.views.index'),
        url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
        url(r'^admin/', include(admin.site.urls)),
    )

Altere-o para delegar as rotas de materiais::

    from django.conf.urls import patterns, include, url

    from django.contrib import admin
    admin.autodiscover()

    urlpatterns = patterns(
        '',
        url(r'^materiais/', include('materiais.urls'), namespace="materiais"),
        url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
        url(r'^admin/', include(admin.site.urls)),
    )


Crie um arquivo de rotas no app materiais, :file:`materiais/urls.py`::

    #coding: utf-8

    from django.conf.urls import patterns, url

    from . import views

    urlpatterns = patterns(
        '',
        url(r'^$', views.index, name='index'),
        url(r'^(?P<material_id>\d+)', views.detalhe, name="detalhe"),
    )


Atualizar templates
-------------------

Agora altere os templates para que as tags ``url`` utilizem o namespace criado.


Arquivo :file:`templates/materiais/index.html`:

.. code-block:: django
    :emphasize-lines: 10

    {% extends "base.html" %}

    {%block content %}
        <h2>Lista de materiais</h2>
        {% if not materiais %}
            <p>Sem materiais para exibir.</p>
        {%else%}
            <ul>
            {% for m in materiais %}
                <li><a href="{% url "materiais:detalhe" m.id %}">{{m.descricao}}</a></li>
            {%endfor%}
            </ul>
        {%endif%}
    {%endblock%}

E no layout base, vamos incluir um link para a lista de materiais. No
arquivo :file:`templates/base.html`, procure a linha:

.. code-block:: django

    <li class="active"><a href="#">Materiais</a></li>

E edite para:

.. code-block:: django

    <li class="active"><a href="{% url "materiais:index" %}">Materiais</a></li>
