
************************************
Class-based views e Views genéricas
************************************

A partir do Django 1.3, surgiram as class-based views, que são uma forma mais
orientada a objetos de construir suas views.

O processo interno continua o mesmo, as rotas continuam recebendo uma função
com a assinatura de uma view::

    def view(request, *args, **kwargs):
        pass

Mas o método da view será gerado a partir de uma classe, que qualquer um
poderia ter construído. Ou seja, as class-based views são opcionais.

No entanto, o conceito é poderoso e pode facilitar o reúso de código em
diversas formas, de modo que algumas views de uso genérico já são distribuídas
com o framework.


View
====

Classe base para as class-based views. Define métodos correspondentes para os
métodos HTTP.


TemplateView
============

Atalho para renderizar um template diretamente.

Vamos utilizar para fazer uma página Home.

View home, em :file:`coolcad/views.py`:

.. code-block:: python

    #coding: utf-8

    from django.views.generic import TemplateView

    class HomeView(TemplateView):
        template_name = 'coolcad/home.html'

Adicionando a view nas rotas, em :file:`coolcad/urls.py`:

.. code-block:: python
    :emphasize-lines: 6, 10

    from django.conf.urls import patterns, include, url

    from django.contrib import admin
    admin.autodiscover()

    from . import views

    urlpatterns = patterns(
        '',
        url(r'^$', views.HomeView.as_view()),
        url(r'^materiais/', include('materiais.urls', namespace="materiais")),
        url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
        url(r'^admin/', include(admin.site.urls)),
    )

E por fim, crie o template :file:`templates/coolcad/home.html`:

.. code-block:: django

    {% extends "base.html" %}

    {% block content %}
    <div class="jumbotron">
            <h1>Coolcad</h1>
            <p class="lead">Bem vindo ao cadastro.</p>
            <p><a class="btn btn-lg btn-success" href="{% url "materiais:index"%}">Cadastre alguma coisa</a></p>
    </div>
    {% endblock content %}


ListView
========

View genérica para listar objetos de um modelo. Tem suporte a paginação.

Vamos refatorar a listagem de materiais.

Edite o arquivo :file:`materiais/views.py`, para importar a view ``ListView``::

    from django.views.generic import ListView

Refatore a view ``index`` para ser uma classe que herde de ListView. Localize o
código::

    def index(request):
        materiais = Material.objects.all()
        context = dict(
            nome="Colibri",
            materiais=materiais
        )
        return render(request, 'materiais/index.html', context)

E altere para::

    class IndexView(ListView):
        model = Material
        context_object_name = 'materiais'
        template_name = 'materiais/index.html'

Vamos alterar a rota para usar a CBV que criamos. Localize o código::

    url(r'^$', views.index, name='index'),

E altere para::

    url(r'^$', views.IndexView.as_view(), name='index'),

