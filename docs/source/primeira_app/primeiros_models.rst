
Primeiros models
================

Os models são as classes do sistema que representam entidades do banco de
dados. O Django possui um sistema para mapeamento do banco em alto nível, onde
podemos descrever características de tabelas e campos através de propriedades
e composição entre classes.

Dentro da nossa app, o primeiro passo será criar os modelos que farão a
persistência dos dados necessários para a app materiais. Vamos começar com
modelos simples para grupos de materiais, e materiais.

.. note::  Aqui utilizaremos vários recursos do ORM do Django. Não se assuste
   com os termos não conhecidos, voltaremos para eles com maior abrangência.
   Para saber mais sobre o ORM Django, consulte a `documentação oficial
   <https://docs.djangoproject.com/en/1.5/topics/db/models/>`_.

Para criar nossos primeiros modelos, edite o arquivo
:file:`materiais/models.py` conforme segue::

    #coding: utf-8
    from django.db import models

    TIPO_GRUPO_CHOICES = (
        (1, u'Alimentos'),
        (2, u'Alcoólicos'),
        (3, u'Não alcoólicos'),
        (4, u'Outros'),
    )

    class GrupoDeMaterial(models.Model):
        nome = models.CharField(max_length=50)
        tipo = models.IntegerField(choices=TIPO_GRUPO_CHOICES)


    class Material(models.Model):
        codigo = models.CharField(u'código', max_length=20)
        descricao = models.CharField(u'descrição', max_length=100)
        unidade = models.CharField(u'unidade', max_length=2)
        grupo = models.ForeignKey(GrupoDeMaterial)

