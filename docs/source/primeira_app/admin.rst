
.. _django-admin:

Habilitando o app Admin
=======================


No arquivo :file:`coolcad/settings.py`, localize e descomente as linhas da aplicação **django.contrib.admin** e **django.contrib.admindocs**::

    INSTALLED_APPS = (
        '<... apps do django ...>',

        'django.contrib.admin',
        'django.contrib.admindocs',

        '<... suas apps ...>',
    )


Edite o arquivo :file:`coolcad/urls.py`::

    from django.conf.urls import patterns, include, url

    # Uncomment the next two lines to enable the admin:
    # from django.contrib import admin
    # admin.autodiscover()

    urlpatterns = patterns('',
        # Examples:
        # url(r'^$', 'coolcad.views.home', name='home'),
        # url(r'^coolcad/', include('coolcad.foo.urls')),

        # Uncomment the admin/doc line below to enable admin documentation:
        # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

        # Uncomment the next line to enable the admin:
        # url(r'^admin/', include(admin.site.urls)),
    )

Localize e descomente as linhas do admin, seu arquivo deve ficar como segue::

    from django.conf.urls import patterns, include, url

    from django.contrib import admin
    admin.autodiscover()

    urlpatterns = patterns('',
        # Examples:
        # url(r'^$', 'coolcad.views.home', name='home'),
        # url(r'^coolcad/', include('coolcad.foo.urls')),

        url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
        url(r'^admin/', include(admin.site.urls)),
    )


Execute o servidor de desenvolvimento::

    (env)$ python manage.py runserver


Aponte o browser para ``localhost:8000/admin``, faça login com seu super
usuário (aquele criado no primeiro ``syncdb``), e verifique os apps
instalados.

Você deve ver ao menos a administração de usuários.


Modelos de materiais no Admin
-----------------------------

No app materiais, no mesmo nível do arquivo :file:`models.py`, crie um arquivo :file:`admin.py`.

Vamos importar o módulo admin, os nossos modelos, e por fim registrar os
modelos no admin::

    #coding: utf-8
    from django.contrib import admin

    from .models import GrupoDeMaterial, Material

    admin.site.register(GrupoDeMaterial)
    admin.site.register(Material)


