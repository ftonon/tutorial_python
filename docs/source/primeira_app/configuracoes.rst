
Configurações
=============

O Django vem com cerca de 219 sabores de configurações para sua escolha. Ao
contrário de frameworks como web2py, algumas configurações não assumem um
valor padrão e precisam serem iniciadas.

Algumas melhores práticas para seguir:

    - Todas as configurações precisam estar no controle de versão.
    - Não se repita (DRY: Don't Repeat Yourself).
    - Mantenha chaves secretas em segurança.


Configurações de caminhos
-------------------------

Mantenha sempre caminhos relativos ao diretório de projeto.

Vamos utilizar a biblioteca `Unipath`_, que faz cálculos de paths de forma elegante e cross-platform.

Instale e atualize o arquivo de dependências. No :term:`repository_root`, execute:

.. code-block:: bash

    (env)$ pip install Unipath

Agora atualize o arquivo de controle das dependências do projeto:

.. code-block:: bash

    (env)$ pip freeze >requirements/base.txt

.. _`Unipath`: https://pypi.python.org/pypi/Unipath

Edite o arquivo de configurações :py:mod:`coolcad/settings`, adicione no início do arquivo.

.. code-block:: python

    #coding: utf-8
    from unipath import Path

    PROJECT_DIR = Path(__file__).ancestor(2)
    getpath = lambda x: PROJECT_DIR.child(x)


Procure e atualize as seguintes configurações:

.. code-block:: python

    ADMINS = (
        ('Seu nome', 'seu_email@example.com'),
    )

    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': getpath('coolcad.db'),
            'USER': '',
            'PASSWORD': '',
            'HOST': '',
            'PORT': '',
        }
    }

    TIME_ZONE = 'America/Sao_Paulo'

    LANGUAGE_CODE = 'pt-br'

    MEDIA_ROOT = getpath('media')

    MEDIA_URL = 'media/'

    STATIC_ROOT = getpath('static')

    STATICFILES_DIRS = (
        getpath('static_site'),
    )

    TEMPLATE_DIRS = (
        getpath('templates'),
    )

Crie os diretórios informados, no :term:`project_root`:

.. code-block:: bash

    $ mkdir media
    $ mkdir static
    $ mkdir static_site
    $ mkdir templates


Crie o banco de dados inicial. Na primeira vez que o comando **syncdb** é
executado, o Django lhe solicita a criação de um super usuário, você pode
informar os valores de sua preferência.

Em um prompt no :term:`project_root`, digite:

.. code-block:: bash

    (env)$ python manage.py syncdb

    Creating tables ...
    Creating table auth_permission
    Creating table auth_group_permissions
    Creating table auth_group
    Creating table auth_user_groups
    Creating table auth_user_user_permissions
    Creating table auth_user
    Creating table django_content_type
    Creating table django_session
    Creating table django_site

    You just installed Django's auth system, which means you don't have any superusers defined.
    Would you like to create one now? (yes/no): yes
    Username (leave blank to use 'fernando'): admin
    Email address: seu_email@example.com
    Password:
    Password (again):
    Superuser created successfully.
    Installing custom SQL ...
    Installing indexes ...
    Installed 0 object(s) from 0 fixture(s)

